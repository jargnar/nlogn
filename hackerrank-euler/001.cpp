/*
Finds the sum of all multiples of 3 or 5 below N.

Author: Suhas <jargnar@gmail.com>
LICENSE: MIT
*/
#include <cstdio>

long sum(long lim, long a) {
    /* We'll use the formula sum = n * (2*a + (n - 1)*d) / 2
    to obtain the sum of the Arithmetic progression.
    In our case, a = d
    */

    // We already have a, and d. Let's find out n
    // There will be limit / a elements in the series
    // We'll be safe and consider up to and not inclusive of limit
    long n = (lim - 1) / a;

    return n * ((2 * a) + ((n - 1) * a)) / 2;
}

int main() {
    int t;
    std::scanf("%d", &t);
    while (t--) {
        long n;
        std::scanf("%ld", &n);
        std::printf("%ld\n", (sum(n, 3) + sum(n, 5)) - sum(n, 15));
    }
    return 0;
}